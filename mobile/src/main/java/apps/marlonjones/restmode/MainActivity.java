package apps.marlonjones.restmode;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
/**
 * @author Marlon Jones (VirusThePanda)
 */
public class MainActivity extends AppCompatActivity {
//TODO: Add in code to save the state of the UI, and make the app well...work xD
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //ImageView and TextView Identification
        final ImageView img1=(ImageView)findViewById(R.id.img1);
        final ImageView img2=(ImageView)findViewById(R.id.img2);
        final TextView s1=(TextView)findViewById(R.id.s1);
        final TextView s2=(TextView)findViewById(R.id.s2);
        final TextView s3=(TextView)findViewById(R.id.s3);
        //OnClick for first
        img1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                img1.setVisibility(View.GONE);
                img2.setVisibility(View.VISIBLE);
                s1.setVisibility(View.GONE);
                s2.setVisibility(View.VISIBLE);
                s3.setVisibility(View.GONE);
            }
        });
        //OnClick for second
        img2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                img2.setVisibility(View.GONE);
                img1.setVisibility(View.VISIBLE);
                s1.setVisibility(View.VISIBLE);
                s2.setVisibility(View.GONE);
                s3.setVisibility(View.GONE);
            }
        });
        //FAB for custom time
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }


}
